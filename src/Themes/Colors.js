export default {
  defaultRefreshSpinner: '#000',
  defaultBackground: '#F0F0F0',
  headerBackground: '#000',
  subHeaderBackground: '#000',
  defaultFilterIcon: '#65c8c6',
  defaultFilterBackground: '#fff',
  defaultFilterText: '#444',
  headerLeftIcon: '#fff',
  pageTitle: '#fff',
  defaultTextColor: '#000',
  defaultTextInputBackgound: '#fff',
  defaultTextInputLabelColor: '#000',
  defaultTextInputColor: '#000',
  defaultPlaceholderTextColor: '#000',
  termAndConditionTextColor: '#000',
  dufaultButtonBackground: '#000',
  defaultButtonText: '#fff',
  defaultAlertTextTitle: '#000',
  defaultAlertTextmessage: '#000',
  defaultCartBarBackground: '#fff',
  btnDone: '#0405cf',// new version

  first: '#fff',
  second: '#000',
  cartBarItems: '#000',
  cartBarItemsActive: '#FF3838',
  flatlistNoItems: '#000',
  red: 'red',
  white: '#fff',
  black: '#000',
  opacity: 'rgba(0,0,0,0.45)',
  gray: 'gray',
  notification: {
    borderColor: '#000',
    backgroundColor: '#f0f0f0',
    textColor: '#000',
    iconColor: '#000',
  },
  firstPage: {
    firstButtonBackground: '#000',
    firstButtonBorder: '#000', //new version
    firstButtonText: '#fff',
    secondButtonBackground: '#fff',
    secondButtonBorder: '#fff',// new version
    secondButtonText: '#000',
    //match button style
    borderMatchButton: '#000',
    backgroundMatchButton: '#f0f0f0',
    textMatchButton: '#000',
  },
  home: {
    name: '#fff',
    description: '#fff',
    menuBackground: '#fff',
    menuIcon: '#000',
    menuDesc: '#000',
    menuText: '#000',
    preferredVenueButton: '#25282a',
    preferredVenueButtonIcon: '#fff',
    tierBackground: '#000',
    iconStar: '#000',
    iconBackground: '#fff',
    venueTitle: '#000',
    venueDesc: '#fff',
    tierBarText: '#fff',
    barcodeGeneratingText: '#ffffff',
    barcodeTopBorder: '#f0f0f0',
    barcodeAreaBackground: '#f0f0f0',
  },
  whaton: {
    tabBackgroundActive: '#000',
    tabBackgroundInactive: '#fff',
    tabTitleActive: '#fff',
    tabTitleInactive: '#000',
    titleColor: '#fff',
    description: '#fff',
    dateColor: '#fff',
  },
  whatonDetails: {
    title: '#000',
    description: '#000',
    dateColor: '#444',
  },
  stampcard: {
    cardTitle: '#000',
    starIcon: '#000',
  },
  giftCertificate: {
    cardTitle: '#fff',
    cardTitleExpanded: '#000',
  },
  buyGiftCertificate: {
    descriptionText: '#fff',
    inputText: '#444',
  },
  about: {
    description: '#444',
  },
  legal: {
    iconArrow: '#000',
    title: '#000',
    description: '#444',
  },
  voucher: {
    title: '#fff',
    date: '#fff',
    description: '#000',
  },
  location: {
    tagsBackgroundActive: '#000',
    tagsBackgroundInactive: '#202224',
    tagsTitleActive: '#000',
    tagsTitleInactive: '#000',
    title: '#000',
    address: '#000',
    description: '#000',
    openHours: '#000',
    cardBackground: '#fff',
    tagsBarBackground: '#f0f0f0',
  },
  faq: {
    searchBox: '#fff',
    title: '#444',
    description: '#444',
    background: '#fff',
  },
  survey: {
    title: '#000',
    description: '#000',
    background: '#fff',
    icon: '#000',
    questionTitle: '#000',
    answer: '#444',
  },
  referFriends: {
    placeHolder: '#444',
    textInput: '#444',
    textInputBackground: '#fff',
  },
  ticket: {
    title: '#fff',
    date: '#fff',
    description: '#000',
  },
  ourTeam: {
    title: '#fff',
    description: '#fff',
    name: '#000',
    background: '#25282a',
    buttonBackground: '#25282a',
    buttonTitle: '#000',
  },
  offer: {
    title: '#fff',
    description: '#fff',
    point: '#fff',
    redeemButtonBackground: '#fff',
    redeemButtonBorder: '#444',
    redeemButtonText: '#444',
    cancelButtonBackground: '#fff',
    cancelButtonBorder: '#444',
    cancelButtonText: '#444',
    popupBackground: '#f0f0f0',
  },
  favorite: {
    title: '#000000',
    description: '#000',
  },
  history: {
    background: '#fff',
    text: '#000',
    filterText: '#000',
  },
  profile: {
    borderButtonColor: '#000',
    backgroundButton: '#f0f0f0',
    buttonText: '#000',
    textInput: '#444',
    icon: '#000',
  },
  resetPassword: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000',
  },
  changeEmail: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000',
  },
  signUp: {
    fieldBackground: '#fff',
    fieldText: '#000',
    backgroundSignupButton: '#000',
    textSignupButton: '#fff',
    placeholderTextColor: '#eee',
  },
  signIn: {
    //forgot password style
    textForgotPassword: '#000',
    //sign in button style
    fieldText: '#444',
    backgroundSigninButton: '#000',
    borderSigninButton: '#000',
    textSigninButton: '#fff',
    placeholderTextColor: '#eee',
    fieldBackground: '#fff',
  },
  preferredVenue: {
    icon: '#fff',
    title: '#fff',
    background: '#202224',
    venueActive: '#000',
    venueActiveText: '#000',
  },
  feedback: {
    description: '#000',
    star: '#000',
    checkBoxOuter: '#000',
    checkBoxInner: '#000',
    background: '#fff',
    textInput: '#000',
  },
  drawer: {
    background: '#25282a',
    title: '#fff',
  },
  tabs: {
    tabBackground: '#fff',
    tabLabel: '#555',
    activeTabLabel: '#000',
  },
  paymentPopup: {
    totalAmount: '#000',
    buttonBackground: '#000',
    buttonText: '#fff',
  },
  forgotPassword: {
    buttonBackground: '#000',
    buttonBorder: '#000',
    buttonText: '#fff',
    placeholderTextColor: '#fff',
  },
  matchAccountScreen: {
    placeholderTextColor: '#eee',
    fieldBackground: '#fff',
    fieldText: '#444',
    backgroundFirstButton: '#000',
    boderColorFirstButton: '#000',
    textFirstButton: '#fff',
    backgroundSecondButton: '#fff',
    boderColorSecondButton: '#000',
    textSecondButton: '#000',
  },
  productItemScreen: {
    cardBackground: '#fff',
    productName: '#fff',
    price: '#fff',
    buttonBackground: '#000',
    buttonBorder: '#fff',
    buttonIcon: '#fff',
    quantityText: '#fff',
  },
  buyGiftCertificateScreen: {
    destColor: '#fff',
  },
  // updated code start from here
  gamingScreen: {
    gameItemBackground: '#25282a',
  },
  membershipScreen: {
    cardTitle: '#65c8c6',
  },
}

