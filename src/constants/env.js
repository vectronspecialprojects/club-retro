import {Platform} from 'react-native';

const buildEvn = {
  production: 'production',
  staging: 'staging',
}

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: '6a66da80-41ae-415f-9391-f2e484039d88',
  buildEvn: buildEvn.staging,
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'HHdi2o7KUl_Qa4HDrs38bBPDjfHospB8O5K_U',
    production: 'ZPxPwnMFZcQaAs4-JkTuVYhVyo_-XIXDTZeAH',
  },
  android: {
    staging: 'sNAhqb2k9uLvAAzx17Ymze7GsnxgGdZa1ZxHE',
    production: 'L_sj-GweHsy8HlkrzLsfGL7UTfIWnUhg97Gn2',
  },
})

export const venueName = 'Club Retro'
export const venueWebsite = 'https://www.clubretro.com.au'

//startup page setup
export const isShowLogo = true

//sign up page setup
export const isMultiVenue = false
export const accountMatch = true

export const isGaming = false //need to be true to allow gamingOdyssey or gamingIGT
export const gamingOdyssey = false
export const gamingIGT = false

export const showTierSignup = false //need to be true to allow isCustomField and showMultipleExtended
export const isCustomField = false //need to be true to allow showMultipleExtended
export const showMultipleExtended = false

//home page setup
export const isShowStarBar = false
export const showTierBelowName = false
export const showPointsBelowName = true
export const isShowPreferredVenue = false
export const showTier = true

export const isGroupFilter = false // must be multi venue and has venue tags
export default vars
