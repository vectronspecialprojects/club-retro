import React, {useEffect, useRef, useState} from 'react'
import {View, ScrollView, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'

function PagingScrollView({
  dotContainerStyle,
  scrollEnabled,
  children,
  allowScrollOverTime,
  dot,
  total,
  activeDot,
  activeDotStyle,
  dotStyle,
  style,
  showDot
}) {
  const [index, setIndex] = useState(0)
  const timeInterval = useRef()
  const scrollRef = useRef()
  const scrollWidth = useRef(0)
  const indexRef = useRef(index)

  useEffect(() => {
    if (allowScrollOverTime) scrollOverTime()
    return () => {
      clearInterval(timeInterval.current)
    }
  }, [])

  const scrollOverTime = () => {
    timeInterval.current = setInterval(() => {
      if (indexRef.current >= total - 1) {
        scrollRef.current?.scrollTo({x: 0, y: 0, animated: true})
        indexRef.current = 0
        setIndex(indexRef.current)
      } else {
        indexRef.current += 1
        scrollRef.current?.scrollTo({x: scrollWidth.current * indexRef.current, y: 0, animated: true})
        setIndex(indexRef.current)
      }
    }, 3000)
  }

  function onScrollEnd(e) {
    let contentOffset = e.nativeEvent.contentOffset
    let viewSize = e.nativeEvent.layoutMeasurement
    indexRef.current = Math.floor(contentOffset.x / viewSize.width)
    setIndex(indexRef.current)
  }

  const renderPagination = () => {
    if (total <= 1) return null
    const dots = []
    const ActiveDot = activeDot || <View style={[styles.dot, styles.activeDot, activeDotStyle]} />
    const Dot = dot || <View style={[styles.dot, dotStyle]} />
    for (let i = 0; i < total; i++) {
      dots.push(i === index ? React.cloneElement(ActiveDot, {key: i}) : React.cloneElement(Dot, {key: i}))
    }
    return dots
  }

  return (
    <View style={[style]}>
      <ScrollView
        ref={scrollRef}
        onLayout={(event) => {
          scrollWidth.current = event.nativeEvent.layout.width
        }}
        horizontal
        pagingEnabled
        keyboardShouldPersistTaps={'handled'}
        scrollEnabled={scrollEnabled}
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={(evt) => {
          onScrollEnd(evt)
        }}>
        {children}
      </ScrollView>
      {showDot && <View style={[styles.dotContainer, dotContainerStyle]}>{renderPagination()}</View>}
    </View>
  )
}

const styles = StyleSheet.create({
  dot: {
    width: responsiveWidth(8),
    height: responsiveWidth(8),
    borderRadius: responsiveWidth(8) / 2,
    marginHorizontal: 5,
    marginVertical: 3,
    backgroundColor: 'gray',
  },
  actionContainer: {
    flexDirection: 'row',
    height: responsiveHeight(40),
    position: 'absolute',
    zIndex: 0,
    bottom: responsiveHeight(20),
    width: '100%',
    justifyContent: 'space-between',
  },
  dotContainer: {
    flexDirection: 'row',
    height: 30,
    alignItems: 'center',
    position: 'absolute',
    zIndex: 0,
    bottom: responsiveHeight(25),
    width: '50%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  actionButton: {
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeDot: {
    backgroundColor: 'white',
  },
})

export default PagingScrollView
