import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Styles from '../Themes/Styles'

const CardHeader = (props) => {
  return (
    <View style={{...Styles.centerContainer, ...props.style}}>
      <Text style={[Styles.header, props.titleStyle]}>{props.title}</Text>
    </View>
  )
}

export default CardHeader
