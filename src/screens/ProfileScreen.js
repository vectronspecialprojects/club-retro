import React, {useCallback, useMemo, useState} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Styles from '../Themes/Styles'
import {useDispatch, useSelector} from 'react-redux'
import Avatar from './../components/Avatar'
import Colors from '../Themes/Colors'
import TextInputView from './../components/TextInputView'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import ButtonView from './../components/ButtonView'
import {updateProfile, updateProfileAvatar} from '../store/actions/infoServices'
import {EntypoTouch} from './../components/UtilityFunctions'
import RouteKey from '../navigation/RouteKey'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {showTier} from '../constants/env'
import {localize} from '../locale/I18nConfig'

const ProfileScreen = (props) => {
  const profile = useSelector((state) => state.infoServices.profile)
  const [dataUpdate, setDataUpdate] = useState({
    first_name: profile?.member?.first_name,
    last_name: profile?.member?.last_name,
    // dob: profile?.member?.dob,
  })
  const [updateImage, setUpdateImage] = useState('')
  const dispatch = useDispatch()

  const handleChangeData = useCallback(
    (key, value) => {
      setDataUpdate({
        ...dataUpdate,
        [key]: value,
      })
    },
    [dataUpdate],
  )

  async function handleUpdateData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      if (isAllowUpdate) await dispatch(updateProfile(dataUpdate, updateImage))
      if (updateImage) {
        await dispatch(updateProfileAvatar(updateImage))
        setUpdateImage('')
      }
      Toast.success(localize('profile.messUpdateSuccess'))
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const isAllowUpdate = useMemo(() => {
    return (
      dataUpdate?.first_name !== profile?.member?.first_name ||
      dataUpdate?.last_name !== profile?.member?.last_name
    )
  }, [profile, dataUpdate])

  return (
    <View style={Styles.screen}>
      <View style={{height: responsiveHeight(100), backgroundColor: Colors.first}}>
        <Avatar
          uri={updateImage?.path || profile?.member?.profile_img}
          size={responsiveWidth(128)}
          style={styles.avatar}
          editable={true}
          onImageSelect={(image) => {
            setUpdateImage(image)
          }}
        />
      </View>
      <View style={styles.actionContainer}>
        <EntypoTouch
          name="heart-outlined"
          size={30}
          color={Colors.profile.icon}
          onPress={() => {
            props.navigation.navigate(RouteKey.FavoriteScreen)
          }}
        />
        <EntypoTouch
          name="back-in-time"
          size={30}
          color={Colors.profile.icon}
          onPress={() => {
            props.navigation.navigate(RouteKey.HistoryScreen)
          }}
        />
      </View>
      <ScrollView>
        <View style={{paddingHorizontal: responsiveWidth(20)}}>
          <TextInputView
            textInputStyle={{color: Colors.profile.textInput}}
            value={dataUpdate?.first_name ?? profile?.member?.first_name}
            placeholder={localize('profile.firstName')}
            onChangeText={(value) => handleChangeData('first_name', value)}
          />
          <TextInputView
            textInputStyle={{color: Colors.profile.textInput}}
            value={dataUpdate?.last_name ?? profile?.member?.last_name}
            placeholder={localize('profile.lastName')}
            onChangeText={(value) => handleChangeData('last_name', value)}
          />
          <TextInputView
            textInputStyle={{color: Colors.profile.textInput}}
            value={profile?.member?.dob}
            placeholder={localize('profile.dob')}
            editable={false}
          />
          <TextInputView
            textInputStyle={{color: Colors.profile.textInput}}
            value={profile?.mobile}
            placeholder={localize('profile.mobile')}
            editable={false}
          />
          <TextInputView
            textInputStyle={{color: Colors.profile.textInput}}
            value={profile?.email}
            placeholder={localize('profile.email')}
            editable={false}
          />
          {showTier && (
            <TextInputView
              textInputStyle={{color: Colors.profile.textInput}}
              value={profile?.member?.member_tier?.tier?.name}
              placeholder={localize('profile.tierName')}
              editable={false}
            />
          )}
          <View style={styles.buttonContainer}>
            <ButtonView
              title={localize('profile.resetPassword')}
              style={styles.buttonBorder}
              titleStyle={{color: Colors.profile.buttonText}}
              onPress={() => {
                props.navigation.navigate(RouteKey.ResetPasswordScreen)
              }}
            />
            <ButtonView
              title={localize('profile.changeEmail')}
              style={styles.buttonBorder}
              titleStyle={{color: Colors.profile.buttonText}}
              onPress={() => {
                props.navigation.navigate(RouteKey.ChangeEmailScreen)
              }}
            />
            <ButtonView
              title={localize('profile.saveChange')}
              disabled={!isAllowUpdate && !updateImage}
              onPress={handleUpdateData}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-64),
    position: 'absolute',
    zIndex: 999,
  },
  buttonBorder: {
    borderWidth: 2,
    borderColor: Colors.profile.borderButtonColor,
    backgroundColor: Colors.profile.backgroundButton,
    marginBottom: responsiveHeight(8),
  },
  buttonContainer: {
    marginTop: responsiveHeight(68),
    marginBottom: responsiveHeight(20),
  },
  actionContainer: {
    height: responsiveHeight(80),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveWidth(20),
    paddingTop: responsiveHeight(10),
  },
})

export default ProfileScreen
