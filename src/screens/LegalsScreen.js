import React, {useState, useEffect, useCallback} from 'react'
import {View, Text, ScrollView, StyleSheet, RefreshControl} from 'react-native'
import {onLinkPress} from './../components/UtilityFunctions'
import {useSelector, useDispatch} from 'react-redux'
import {TouchableCmp} from './../components/UtilityFunctions'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import Html from '../components/Html'
import SubHeaderBar from './../components/SubHeaderBar'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Fonts from '../Themes/Fonts'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig';

const LegalsScreen = (props) => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isGroupShown, setIsGroupShown] = useState('')
  const legals = useSelector((state) => state.infoServices.legals)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchLegals)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    setIsRefreshing(false)
    dispatch(setGlobalIndicatorVisibility(false))
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const toggleGroup = (title) => {
    if (isGroupShown === title) {
      setIsGroupShown('')
    } else {
      setIsGroupShown(title)
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <View style={{flex: 1, padding: 20}}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
              tintColor={Colors.defaultRefreshSpinner}
              titleColor={Colors.defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }>
          {legals.map((legal) => {
            return (
              <View style={{paddingHorizontal: 5}} key={legal[0]}>
                <TouchableCmp activeOpacity={0.6} onPress={toggleGroup.bind(this, legal[0])}>
                  <View style={styles.titleContainer}>
                    <Text style={styles.title}>{legal[0]}</Text>
                    <AntDesign
                      name={isGroupShown === legal[0] ? 'down' : 'right'}
                      size={22}
                      color={Colors.legal.iconArrow}
                    />
                  </View>
                </TouchableCmp>
                {isGroupShown === legal[0] ? (
                  <View style={{marginBottom: 0}}>
                    <Html
                      html={legal[1]}
                      textAlign={'left'}
                      color={Colors.legal.description}
                      onLinkPress={onLinkPress}
                    />
                  </View>
                ) : null}
              </View>
            )
          })}
        </ScrollView>
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    color: Colors.legal.title,
    fontFamily: Fonts.openSansBold,
    fontSize: 20,
    textAlign: 'left',
  },
})

export default LegalsScreen
