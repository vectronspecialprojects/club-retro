import React from 'react'
import {View, SafeAreaView, Image, ScrollView, StyleSheet} from 'react-native'
import Images from '../../Themes/Images';

export const DrawerLogo = (props) => (
  <View style={{flex: 1}}>
    <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
      <View style={styles.drawerLogoContainer}>
        <Image source={Images.logoDrawer} style={styles.drawerLogo} />
      </View>
      <ScrollView>{props.children}</ScrollView>
    </SafeAreaView>
  </View>
)

const styles = StyleSheet.create({
  drawerLogoContainer: {
    height: 100,
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerLogo: {
    width: '50%',
    resizeMode: 'contain',
  },
})
